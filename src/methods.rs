//! Vote counting methods

pub mod alternative;
pub mod baldwin;
pub mod borda;
pub mod irv;
pub mod meek;
pub mod minimax;
pub mod nanson;
pub mod ranked_pairs;
pub mod schulze;
pub mod smith_schwartz;

pub use alternative::Alternative;
pub use baldwin::Baldwin;
pub use borda::Borda;
pub use irv::Irv;
pub use meek::Meek;
pub use minimax::Minimax;
pub use nanson::Nanson;
pub use ranked_pairs::RankedPairs;
pub use schulze::Schulze;
pub use smith_schwartz::schwartz_set;
pub use smith_schwartz::smith_set;
