#[derive(Clone, Copy)]
pub struct CandPair {
    pub runner: u32,
    pub opponent: u32,
    score: u64,
    opposition: u64,
}

impl CandPair {
    pub fn new(runner: u32, opponent: u32, score: u64, opposition: u64) -> CandPair {
        CandPair {
            runner,
            opponent,
            score,
            opposition,
        }
    }
}

use std::cmp::{Eq, Ordering, PartialEq, PartialOrd};
impl PartialOrd for CandPair {
    fn partial_cmp(&self, other: &CandPair) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
//Note that this order is reverse in opposition
impl Ord for CandPair {
    fn cmp(&self, other: &CandPair) -> Ordering {
        match self.score.cmp(&other.score) {
            Ordering::Equal => other.opposition.cmp(&self.opposition),
            x => x,
        }
    }
}
impl PartialEq for CandPair {
    fn eq(&self, other: &CandPair) -> bool {
        self.score == other.score && self.opposition == other.opposition
    }
}
impl Eq for CandPair {}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn cand_pair() {
        let a = CandPair {
            runner: 1,
            opponent: 2,
            score: 11,
            opposition: 7,
        };
        let b = CandPair {
            runner: 3,
            opponent: 4,
            score: 11,
            opposition: 9,
        };
        let c = CandPair {
            runner: 5,
            opponent: 6,
            score: 11,
            opposition: 7,
        };
        let d = CandPair {
            runner: 7,
            opponent: 8,
            score: 21,
            opposition: 7,
        };
        assert!(a == c);
        assert!(a > b);
        assert!(d > a);
    }
}
