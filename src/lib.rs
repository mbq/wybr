pub mod common;
mod io;
pub mod methods;
pub mod outcome;
mod prng;
pub mod tally;

pub use common::{BordaKind, ElectionError, Outcome, PairScore, Quota, SetType, Transfer};
pub use tally::{Tally, VoteMatrix, VoteTree};

pub use methods::{
    schwartz_set, smith_set, Alternative, Baldwin, Borda, Irv, Meek, Minimax, Nanson, RankedPairs,
    Schulze,
};
