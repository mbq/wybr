# Wybr

[![codecov](https://codecov.io/gl/mbq/wybr/branch/master/graph/badge.svg)](https://codecov.io/gl/mbq/wybr)
[![pipeline status](https://gitlab.com/mbq/wybr/badges/master/pipeline.svg)](https://gitlab.com/mbq/wybr/commits/master)
[![docs](https://docs.rs/wybr/badge.svg)](https://docs.rs/wybr)

Wybr is a yet another Rust library implementing some methods of counting votes in preferential elections.

## Capabilities

- Read a blt file, as specified in [Algorithm 123 paper](http://www.dia.govt.nz/diawebsite.NSF/Files/meekm/%24file/meekm.pdf) with OpenSTV extensions for equal votes.
- [Tideman alternative count](https://en.wikipedia.org/wiki/Tideman_alternative_method) with common variants.
- [Borda count](https://en.wikipedia.org/wiki/Borda_count) with common variants, also usable as plurality voting.
- [Nanson method](https://en.wikipedia.org/wiki/Nanson%27s_method).
- [Baldwin method](https://en.wikipedia.org/wiki/Nanson%27s_method).
- [Instant run-off](https://en.wikipedia.org/wiki/Instant-runoff_voting).
- [Meek STV count](http://www.dia.govt.nz/diawebsite.NSF/Files/meekm/%24file/meekm.pdf) with fixed-point arithmetic, also with Warren variant and few quota options.
- [Minimax count](https://en.wikipedia.org/wiki/Minimax_Condorcet_method).
- [Tideman ranked pairs](https://en.wikipedia.org/wiki/Ranked_pairs) method with common variants.
- [Schulze count](https://en.wikipedia.org/wiki/Schulze_method), in a basic, but most popular form (without advanced tie-breaking).
- Find [Smith](https://en.wikipedia.org/wiki/Smith_set) and [Schwartz](https://en.wikipedia.org/wiki/Schwartz_set) sets.
- Uses pseudo-random tie breaking in a reproducible way and inform user if the count is deterministic.
- Usable (hopefully) errors, no by-design panics.

## Ugly parts

- Not battle tested nor audited.
- Fixed types for candidates and scores.
- Inefficient algorithms and data structures here and there.
- Hobby project.
- Poor documentation.

## Related crates

- [good_stv](https://crates.io/crates/good_stv)
- [tallyman](https://crates.io/crates/tallyman)

